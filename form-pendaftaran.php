<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .color{
            background-color: red;
            color: white;
        }
        .border{
            border: 1px solid black;
        }
    </style>
</head>
<body>
    <table class="border" cellspacing="1" cellpadding="8">
        <th  colspan=2 bgcolor=red> <font color = white size = 5>Form Pendaftaran KTP</font></th>
        <form action="simpan-data.php" method="post">
            <tr>
                <td>Nomor Pendaftaran</td>
                <td>
                <input  type=text name="nomor_pendaftaran"></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><input size= 48 type=text placeholder= "Nama Lengkap" name="nama"></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>
                <textarea name="alamat" cols=50 row =5 placeholder="Alamat Lengkap" ></textarea>
                </td>
            </tr>
            <tr>
                <td>Provinsi</td>
                <td>
                <select name ="provinsi">
                <option value="Sulawesi selatan" >Sulawesi Selatan</option>
                <option value="Sulawesi tengah">Sulawesi Tengah</option>
                </select>
                </td>
            </tr>
            <tr>
                <td>Kota</td>
                <td>
                    <input type="text" size="8" placeholder="Kota" name="kota">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kec. &nbsp;&nbsp;
                    <input type="text" size="8" placeholder="Kacamatan" name="kacamatan">
                </td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td>
                    <input type="text" placeholder="Pekerjaan" name="pekerjaan">
                </td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>
                    <input type="radio" name="jenis_kelamin" value="Pria"> Pria &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="jenis_kelamin" value="Wanita">  Wanita
                </td>
            </tr>
            <tr>
                <td>Tempat/Tanggal Lahir</td>
                <td>
                    <input type="text" size="8" placeholder="Tempat Lahir" name="tempat_lahir">
                    <input type="date" name="tanggal">
                </td>
            </tr>
            <tr>
                <td rowspan="2">Kelengkapan Berkas</td>
                <td>
                    <input type="checkbox" value="lengkap" name="FC_KK"> FC. kk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="checkbox" value="lengkap" name="Surat_pengantar"> Surat Pengantar
                </td>
                <tr>
                    <td>
                        <input type="checkbox" name="Pas_poto" value="lengkap"> Pas Foto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="checkbox" name="Surat_domisili" value="lengkap"> Surat Domisili
                    </td>
                </tr>
            </tr>
            <tr>
                <td>File Scan</td>
                <td>
                    <input type="file" name="file_dokumen" name="lengkap">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value=submit>
                    <input type="Reset" value=Batal>
                </td>
            </tr>
        </form>
    </table>
</body>
</html>